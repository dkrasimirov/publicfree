
# CoverageReport
[![coverage report](https://gitlab.com/dkrasimirov/publicfree/badges/main/coverage.svg)](https://gitlab.com/dkrasimirov/publicfree/-/commits/main) 

## Step 1 - investigating he application in terms of how it works
* Investigating the source code and the following os.Getenv("REDIS_URL"), and os.Getenv("SERVICE_ADDR"). Thoose are ENV expected by the application to be able to work. 
* Thinking how the Dockerfile should look like - it have to accept some arguments with ARG passing them during the run with docker -e ARG1 - e ARG2
*  Thinking about the dependency like Redis in this case, so I need to see how exactly to pass REDIS_URL. 
*  So a little bit of digging in the code to see in the file go.mod and see the exact module for redis
github.com/go-redis/redis v6.15.9+incompatible // indirect. So Basically I saw what parameter it is expected for REDIS_URL.. 


## step 2 - Create DockerFile and test the containers locally

* Install Docker desktop on a local computer 
* Create the docker file (Dockerfile_single_anotherway). This is not multistage build but works for quick test on a local env.  The agruments passed become ENV variables and it is what the application expects. First one is the app URL and port second is the connectivity to the redis container.
* Do a local docker build and test of the containers (locally I wanted first to build the container like in a local environment and see how it works. Then I ran first redis container and then build and test the custom go application and how it is connecting to redis and how it works at all.)
* Run redis container docker run --name some-redis -p 6379:6379 -d redis
* docker build -t go3 .
```
docker run  -p 8080:8080 -e SERVICE_ADDR=0.0.0.0:8080 -e REDIS_URL=<my_ip>:6379 -d go3
```
(there was some try and error but I figured out and made working solution locally). It can be done also with docker compose to test which might be good solution as well.)
* Createte multi-stage dockerfile to only copy the binay.



## Step 3 - Create a pipiline skeleton

* Opening account in Gitlab, Creating a new blank project and then clone code for the go application from github.
* Work in my case with VSCode locally and then commit to the Gitlab in the new created project repository. 
* Crete the .gitlab-ci.yml file.
* Create the unit and code coverage test stage running in paralleland then the docker build and push stage.
* use default Gitlab templates and some research and base on them create the gitlab CI/CD pipeline with test and build stage
* Commit the code


## Step 4 - Test the pipeline

* Test the pipeline running it (auto trigger) and observing the pipeline jobs output for the test and the docker build and publish step.
* Verifying the gitlab container registry.

## Step 5 - Create Kubernetes yaml file and test the deployment in Google Cloud Platform and test the application
Create GKE with basic command line 
```
gcloud container clusters create mycluster --zone europe-west3-a

```
or
```
gcloud container clusters create mycluster --disk-size=10GB --disk-type==pd-standard --zone europe-west3-a --machine-type=e2-medium --cluster-version=1.21.5-gke.1302 --num-nodes=3
```
```
gcloud container clusters get-credentials mycluster --zone europe-west3-a
```
After that apply the yaml file. It is only one file because the deployment is not very complicated. 

* a. create test namespace
* b. Create the deployment for main application with 1 replica with the respective lables in the test namespace
* c. Create the deployment for redis with one replica in the test namespace
* d. Create service for redis with the respective selectors of type clusterIP
* e. Create Loadbalancer type of service to publish the application (for this in GCP you can use kubectl expose deployment with --dry-run=client -o yaml to get the yaml file which is specific for loadbalancing in GCP)
* adding requests and limits to the deployments (always good to have)
inspect the logs for the pods and test that application is running on the public IP given by the loadbalancer service.



## Optional verification and troubleshooting
```
kubectl apply -f kubernetes-deployment.yaml
kubectl get pods -n test
kubectl get deployments -n test
kubectl describe deployment goapp -n test
kubectl logs <pod_name> -n test
kubectl get svc -n test
kubectl describe svc redis -n test
kubecctl describe svc goapp -n test
```
