#This is multistage Docker build 
FROM golang:1.16.11-alpine3.15 as builder
WORKDIR /build
COPY go.mod go.sum ./
# Download all dependencies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go mod download
COPY . .
RUN go build -o main .
 
# This are the input arguments
ARG SERVICE_ADDR=$address
ARG REDIS_URL=$redis

ENV REDIS_URL=$REDIS_URL
ENV SERVICE_ADDR=$SERVICE_ADDR

from golang:1.16.11-alpine3.15
# Add Maintainer Info
LABEL maintainer="Danail Surudzhiyski"


# Set the Current Working Directory inside the container
WORKDIR /app

# Copy only the executable binary from previous build
COPY --from=builder /build/main .

# Expose port 8080 to the outside world
EXPOSE $SERVICE_ADDR

# Command to run the executable
CMD ["./main"]
